clc;clear;close all;warning off all;

load data_latih.mat;
load target_latih.mat;
load target_asli_latih;

% Pembuatan JST
net = newff(minmax(data_latih),[10 1],{'logsig','purelin'},'traingdx');

% Memberikan nilai untuk mempengaruhi proses pelatihan
net.performFcn = 'mse';
net.trainParam.goal = 0.001;
net.trainParam.show = 25;
net.trainParam.epochs = 5000;
net.trainParam.mc = 0.8;
net.trainParam.lr = 0.9;

% Proses training
[net_keluaran,tr,Y,E] = train(net,data_latih,target_latih);

% Hasil setelah pelatihan
[m,n] = size(data_latih);
bobot_hidden = net_keluaran.IW{1,1};
bobot_keluaran = net_keluaran.LW{2,1};
bias_hidden = net_keluaran.b{1,1};
bias_keluaran = net_keluaran.b{2,1};
jumlah_iterasi = tr.num_epochs;
nilai_keluaran = Y;
nilai_error = E;
error_MSE = (1/n)*sum(nilai_error.^2);

save net.mat net_keluaran

% Hasil prediksi
hasil_latih = sim(net_keluaran,data_latih);
max_data = 517;
min_data = 350;
hasil_latih = ((hasil_latih-0.1)*(max_data-min_data)/0.8)+min_data;

% Performansi hasil prediksi
figure,
plotregression(target_asli_latih,hasil_latih,'Regression')

figure,
plotperform(tr)

figure,
plot(hasil_latih,'bo-')
hold on
plot(target_asli_latih,'ro-')
hold off
grid on
title(strcat(['Grafik Keluaran JST vs Target dengan nilai MSE = ',...
    num2str(error_MSE)]))
xlabel('Pola ke-')
ylabel('TOEFL')
legend('Keluaran JST','Target','Location','Best')