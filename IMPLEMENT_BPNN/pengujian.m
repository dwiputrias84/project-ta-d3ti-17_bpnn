clc;clear;close all;warning off all;

% load jaringan yang sudah dibuat pada proses pelatihan
load net.mat
load data_uji.mat;
load target_uji.mat;
load target_asli_uji;
[m,n] = size(data_uji);

% Pembuatan JST
%net = newff(minmax(data_uji),[10 1],{'logsig','purelin'},'traingdx');

% Memberikan nilai untuk mempengaruhi proses pelatihan
% net.performFcn = 'mse';
% net.trainParam.goal = 0.001;
% net.trainParam.show = 25;
% net.trainParam.epochs = 5000;
% net.trainParam.mc = 0.8;
% net.trainParam.lr = 0.9;

% Proses testing
%[net_keluaran,tr,Y,E] = test(net,data_uji,target_uji);
%[net_keluaran,tr,Y,E] = train(net,data_latih,target_latih);

% Hasil prediksi
hasil_uji = sim(net_keluaran,data_uji);
nilai_error = hasil_uji-target_uji;

max_data = 517;
min_data = 350;
hasil_uji = ((hasil_uji-0.1)*(max_data-min_data)/0.8)+min_data;

% Performansi hasil prediksi
error_MSE = (1/n)*sum(nilai_error.^2);

figure,
plotregression(target_asli_uji,hasil_uji,'Regression')

figure,
plot(hasil_uji,'bo-')
hold on
plot(target_asli_uji,'ro-')
hold off
grid on
title(strcat(['Grafik Keluaran JST vs Target dengan nilai MSE = ',...
    num2str(error_MSE)]))
xlabel('Pola ke-')
ylabel('TOEFL')
legend('Keluaran JST','Target','Location','Best')
