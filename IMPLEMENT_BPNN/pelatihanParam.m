e_epoch = 5000;
e_goals = 0.001;
e_momentum = 0.8;


learningRate =[0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9];
e_hidden = 10;
for i=1:length(learningRate)
    disp('Pelatihan ini menggunakan Learning Rate');
    disp(learningRate(i));
    net=newff(minmax(data_latih),[e_hidden, 1],{'logsig','logsig'},'traingd');
    net.performFcn = 'mse';
    net.trainparam.epochs=e_epoch;
    net.trainparam.goal=e_goals;
    net.trainparam.mc=e_momentum;
    net.trainparam.lr=learningRate(i);
    net=init(net);
    [net,tr]=train(net,data_latih,target_latih);

end